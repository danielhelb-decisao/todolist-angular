import express from "express";
import ListController from "./controllers/ListController";
import UserController from "./controllers/UserController";

const router = express.Router();

router.post("/todolist", ListController.create);
router.get("/todolist", ListController.select);
router.get("/todolist/atividades", ListController.selectConclusao);
router.get("/todolist/:id", ListController.selectOne);
router.put("/todolist/:id", ListController.update);
router.delete("/todolist/:id", ListController.delete);

router.post("/create-user", UserController.create);
router.get("/login", UserController.select);
router.put("/login", UserController.login);
router.put("/login-validate", UserController.validarSenhaFrontend);

export { router };
