import { Atividade } from './../../atividade';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario, UsuarioLogado } from 'src/app/usuario';
import { take } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  usuario!: Usuario[];
  private readonly API_LOGIN = 'http://localhost:3001/login';
  private readonly API_CREATE_LOGIN = 'http://localhost:3001/create-user';
  private readonly API_PASSWORD_CRYPTO = 'http://localhost:3001/login-validate';
  header = { 'Content-Type': 'application/json' };

  constructor(private http: HttpClient) {}

  criarLogin(usuario: Usuario) {
    return this.http
      .post<Usuario>(this.API_CREATE_LOGIN, usuario, { headers: this.header })
      .pipe(take(1));
  }

  entrar(usuario: Usuario) {
    return this.http
      .put(this.API_LOGIN, usuario, { headers: this.header })
      .pipe(take(1));
  }

  loginExiste(usuario: Usuario) {
    return this.http
      .get(`${this.API_LOGIN}?email=${usuario.email}`)
      .pipe(take(1));
  }

  validacaoSenhaCrypto(senha: string) {
    return this.http
      .put<string>(
        this.API_PASSWORD_CRYPTO,
        { senha },
        { headers: this.header }
      )
      .pipe(take(1));
  }

  usuarioLogado(email: string, usuario: UsuarioLogado) {
    return this.http
      .put<Atividade>(`${this.API_LOGIN}?email=${email}`, usuario, {
        headers: this.header,
      })
      .pipe(take(1));
  }

  selectUsuarioLogado(usuario: Usuario) {
    this.loginExiste(usuario).subscribe((usuario) => {
      let email = Object.values(usuario)[1];
      let logado = Object.values(usuario)[3];
      return { email, logado };
    });
  }
}
