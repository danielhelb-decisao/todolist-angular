export interface Atividade {
  id: number;
  atividade: string;
  concluido: boolean;
  dataConclusao: Date;
  dataCriacao: Date;
}

export interface AtividadeCreate {
  atividade: string;
  concluido: boolean;
  dataConclusao: Date;
}

export interface AtividadeUpdate {
  concluido: boolean;
  dataConclusao: Date | null;
}

export interface Update {
  atividade: string;
}
