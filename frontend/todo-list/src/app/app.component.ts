import { AccountService } from './atividades/shared/account.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Usuario } from './usuario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'todo-list';
  usuario!: Usuario;

  constructor(private router: Router, private loginService: AccountService) {}

  ngOnInit(): void {
    // this.router.navigate(['/login'])
  }

  Sair(usuario: Usuario) {
    this.loginService.selectUsuarioLogado(usuario);
  }
}
