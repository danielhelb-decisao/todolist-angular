export interface Usuario {
  email: string;
  senha: string;
  logado: boolean;
}

export interface UsuarioLogado {
  logado: boolean;
}
